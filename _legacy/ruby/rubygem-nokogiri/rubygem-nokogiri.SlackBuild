#!/bin/sh

# Slackware build script for nokogiri

# Copyright 2016 Marek Buras <mz82kb (at) fastmail.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

PRGNAM=rubygem-nokogiri
VERSION=${VERSION:-1.6.7.2}
BUILD=${BUILD:-1}
TAG=${TAG:-_MaBu}

SRCNAM=nokogiri

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i486 ;;
    arm*) ARCH=arm ;;
# Unless $ARCH is already set, use uname -m for all other archs:
       *) ARCH=$( uname -m ) ;;
  esac
fi

CWD=$(pwd)
TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

if [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e 

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP

# Get the full path of the ruby installation, for gems
DESTDIR=$( ruby -r rbconfig -e '
include RbConfig
printf("%s/%s/gems/%s\n",
	CONFIG["libdir"],
	CONFIG["RUBY_INSTALL_NAME"],
	CONFIG["ruby_version"]
      )
')

gem install \
	--local \
	--no-update-sources \
	--ignore-dependencies \
	--backtrace \
	--install-dir $PKG/$DESTDIR \
	--bindir $PKG/usr/bin \
	$CWD/$SRCNAM-$VERSION.gem

# Strip binaries and libraries.
find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

# Remove cached gem from install, if you are so inspired ..
rm -rf $PKG/$DESTDIR/cache

# Copy program documentation into the package
# Also, include the SlackBuild script in the documentation directory
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
tar -x -O --file=$CWD/$SRCNAM-$VERSION.gem data.tar.gz | tar -xz -C $PKG/usr/doc/$PRGNAM-$VERSION --file=- \
	CHANGELOG.rdoc LICENSE.txt README.md ROADMAP.md
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

# Copy the slack-desc (and a custom doinst.sh if necessary) into ./install
mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/doinst.sh > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tgz}
