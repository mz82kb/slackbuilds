#!/bin/sh

JAVA_HOME=/opt/icedtea7-jdk
export JAVA_HOME

PATH=$PATH:$JAVA_HOME/bin
export PATH