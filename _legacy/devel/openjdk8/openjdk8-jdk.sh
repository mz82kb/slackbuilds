#!/bin/sh

JAVA_HOME=/opt/openjdk8-jdk
export JAVA_HOME

PATH=$PATH:$JAVA_HOME/bin
export PATH
