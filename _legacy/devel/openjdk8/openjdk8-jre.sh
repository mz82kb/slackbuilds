#!/bin/sh

JAVA_HOME=/opt/openjdk8-jre
export JAVA_HOME

PATH=$PATH:$JAVA_HOME/bin
export PATH
